﻿using System;
using System.Collections.Generic;

namespace RazorPagePractice.Models
{
    public partial class Subject
    {
        public Subject()
        {
            StudentRecords = new HashSet<StudentRecord>();
        }

        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public int Slot { get; set; }

        public virtual ICollection<StudentRecord> StudentRecords { get; set; }
    }
}
