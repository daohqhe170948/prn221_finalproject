﻿using System;
using System.Collections.Generic;

namespace RazorPagePractice.Models
{
    public partial class StudentRecord
    {
        public int Id { get; set; }
        public int? StudentId { get; set; }
        public int? SubjectId { get; set; }
        public double Mark { get; set; }

        public virtual Student? Student { get; set; }
        public virtual Subject? Subject { get; set; }
    }
}
