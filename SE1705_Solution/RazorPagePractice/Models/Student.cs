﻿using System;
using System.Collections.Generic;

namespace RazorPagePractice.Models
{
    public partial class Student
    {
        public Student()
        {
            StudentRecords = new HashSet<StudentRecord>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public DateTime Dob { get; set; }

        public virtual ICollection<StudentRecord> StudentRecords { get; set; }
    }
}
