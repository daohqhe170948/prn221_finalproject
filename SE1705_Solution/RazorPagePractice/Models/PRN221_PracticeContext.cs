﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RazorPagePractice.Models
{
    public partial class PRN221_PracticeContext : DbContext
    {
        public PRN221_PracticeContext()
        {
        }

        public PRN221_PracticeContext(DbContextOptions<PRN221_PracticeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Student> Students { get; set; } = null!;
        public virtual DbSet<StudentRecord> StudentRecords { get; set; } = null!;
        public virtual DbSet<Subject> Subjects { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("Student");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<StudentRecord>(entity =>
            {
                entity.ToTable("StudentRecord");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.StudentId).HasColumnName("StudentID");

                entity.Property(e => e.SubjectId).HasColumnName("SubjectID");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentRecords)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__StudentRe__Stude__3B75D760");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.StudentRecords)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK__StudentRe__Subje__3C69FB99");
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.ToTable("Subject");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Title).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
