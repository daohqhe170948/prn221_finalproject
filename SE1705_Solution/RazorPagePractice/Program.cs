using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using RazorPagePractice.Models;

namespace RazorPagePractice
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddDbContext<PRN221_PracticeContext>(options =>
            {
                options.UseSqlServer("Server=LAPTOP-2FLOH7TF; database=PRN221_Practice; uid=ned; pwd=27062003x; Encrypt=true; TrustServerCertificate=true");
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapRazorPages();

            app.Use((context, next) =>
            {
                // Clear TempData when the application starts
                var tempDataProvider = context.RequestServices.GetRequiredService<ITempDataProvider>();
                tempDataProvider.LoadTempData(context);
                tempDataProvider.SaveTempData(context, new TempDataDictionary(context, tempDataProvider));

                return next();
            });

            app.Run();
        }
    }
}