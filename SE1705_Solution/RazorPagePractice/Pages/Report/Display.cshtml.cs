using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.EntityFrameworkCore;
using RazorPagePractice.Models;

namespace RazorPagePractice.Pages.Report
{
    public class DisplayModel : PageModel
    {
        private PRN221_PracticeContext DbContext;
        public List<Student> Students;
        public List<Subject> Subjects;
        public List<StudentRecord> StudentRecords;

        [BindProperty(SupportsGet = true)]
        public string test_string { get; set; }

        public DisplayModel(PRN221_PracticeContext context)
        {
            DbContext = context;
        }

        public void OnGet()
        {
            Students = DbContext.Students.ToList();
            Subjects = DbContext.Subjects.ToList();
            StudentRecords = DbContext.StudentRecords.ToList();
            test_string = TempData["filter"] as string ?? string.Empty;
        }

        public void OnPost()
        {
            Students = DbContext.Students.ToList();
            Subjects = DbContext.Subjects.ToList();
            StudentRecords = DbContext.StudentRecords.ToList();
        }
    }
}
