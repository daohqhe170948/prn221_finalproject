﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    class EmployeeVM
    {
        public List<Employee> list { get; set; }
        public EmployeeVM()
        {
            list = new List<Employee>
            {
                new Employee{Id = 1, Name="chilp" },
                new Employee{Id = 2, Name="sonnt" }
            };
        }
    }
}
