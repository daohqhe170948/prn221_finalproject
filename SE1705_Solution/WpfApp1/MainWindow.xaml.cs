﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            label1.Content = "Content 1 moi";

            //tao doi tuong Binding = code C#
            Binding binding = new Binding();
            binding.Source = textbox3;//binding.ElementName = "textbox3";
            binding.Mode= BindingMode.OneWay;
            binding.Path = new PropertyPath("Text");
            textbox4.SetBinding(TextBox.TextProperty, binding); //xac dinh target

            Employee e = new Employee { Id = 12, Name = "abc" };
            mystack1.DataContext = e;

        }
    }
}
