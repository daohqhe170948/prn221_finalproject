﻿using Lab2_Client.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab1.ViewModels
{
    public class BaseVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public TcpClient Client { get; set; }
        public NetworkStream Stream { get; set; }
        public string ServerIP { get; set; }
        public int ServerPort { get; set; }

        protected virtual void InvokeChanged([CallerMemberName] string propertyName = null)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ConnectToServer()
        {
            try
            {
                Client = new TcpClient(ServerIP, ServerPort);
                Stream = Client.GetStream();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot connect to server");
            }
        }

        public string? GetMessage(string requestMessage)
        {
            if (Client.Connected)
            {
                //Send request message
                byte[] data = Encoding.ASCII.GetBytes(requestMessage);
                Stream.Write(data, 0, data.Length);

                //Receive response message
                data = new byte[1024];
                StringBuilder sb = new StringBuilder();
                int bytesRead;
                do
                {
                    bytesRead = Stream.Read(data, 0, data.Length);
                    sb.Append(Encoding.ASCII.GetString(data, 0, bytesRead));
                } while (Stream.DataAvailable);
                return sb.ToString();
            }
            return null;
        }
    }
}
