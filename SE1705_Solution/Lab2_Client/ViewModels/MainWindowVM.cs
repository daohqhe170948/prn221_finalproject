﻿using Lab1.ViewModels;
using Lab2_Client.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using GalaSoft.MvvmLight.Command;

namespace Lab2_Client.ViewModels
{
    public class MainWindowVM : BaseVM
    {
        private IConfiguration Configuration { get; set; }

        private List<Product> _products;
        private Product? _selectedProduct;
        private Product? _validateProduct;
        private bool _isPopupOpen;
        private string? _searchID;

        private RelayCommand _exitTabCommand;
        private RelayCommand _createProductCommand;
        private RelayCommand _deleteProductCommand;
        private RelayCommand _editProductCommand;
        private RelayCommand _createButtonPressCommand;
        private RelayCommand _searchButtonCommand;
        private RelayCommand _refreshButtonCommand;

        public List<Product> Products
        {
            get => _products;
            set
            {
                _products = value;
                InvokeChanged(nameof(Products));
            }
        }
        public Product? SelectedProduct
        {
            get => _selectedProduct;
            set
            {
                _selectedProduct = value;
                if (value != null)
                {
                    IsPopupOpen = true;
                    ValidateProduct = value;
                }
                InvokeChanged(nameof(SelectedProduct));
            }
        }
        public Product? ValidateProduct
        {
            get => _validateProduct;
            set
            {
                _validateProduct = value;
                InvokeChanged(nameof(ValidateProduct));
            }
        }
        public bool IsPopupOpen
        {
            get => _isPopupOpen;
            set
            {
                _isPopupOpen = value;
                InvokeChanged(nameof(IsPopupOpen));
            }
        }
        public string? SearchID
        {
            get => _searchID;
            set
            {
                _searchID = value;
                InvokeChanged(nameof(SearchID));
            }
        }

        public RelayCommand ExitTabCommand { get; set; }
        public RelayCommand CreateProductCommand { get; set; }
        public RelayCommand DeleteProductCommand { get; set; }
        public RelayCommand EditProductCommand { get; set; }
        public RelayCommand CreateButtonPressCommand { get; set; }
        public RelayCommand SearchButtonCommand { get; set; }
        public RelayCommand RefreshButtonCommand { get; set; }

        public MainWindowVM()
        {
            Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            ServerIP = Configuration["ServerConnection:ServerIP"] ?? String.Empty;
            ServerPort = Int32.Parse(Configuration["ServerConnection:ServerPort"] ?? String.Empty);

            IsPopupOpen = false;
            SelectedProduct = null;
            ValidateProduct = null;
            SearchID = null;

            ConnectToServer();

            //Load List Product
            Products = GetProducts().Data;

            ExitTabCommand = new RelayCommand(OnExitButtonClick);
            DeleteProductCommand = new RelayCommand(OnDeleteButtonClick);
            CreateProductCommand = new RelayCommand(OnCreateButtonConfirmClick);
            EditProductCommand = new RelayCommand(OnUpdateButtonClick);
            CreateButtonPressCommand = new RelayCommand(OnCreateButtonClick);
            SearchButtonCommand = new RelayCommand(OnSearchButtonClick);
            RefreshButtonCommand = new RelayCommand(() => Products = GetProducts().Data);
        }


        public void OnExitButtonClick()
        {
            IsPopupOpen = false;
            SelectedProduct = null;
        }

        public void OnCreateButtonClick()
        {
            IsPopupOpen = true;
            SelectedProduct = new Product();
        }

        public void OnCreateButtonConfirmClick()
        {
            var result = CreateProduct().Message;
            IsPopupOpen = false;
            Products = GetProducts().Data;
            SelectedProduct = null;
            MessageBox.Show(result);
        }

        public void OnDeleteButtonClick()
        {
            var result = DeleteProduct().Message;
            IsPopupOpen = false;
            Products = GetProducts().Data;
            SelectedProduct = null;
            MessageBox.Show(result);
        }

        public void OnSearchButtonClick()
        {
            var result = SearchProduct(SearchID);
            var searchProduct = result.Data == null ? new List<Product>() : new List<Product> { result.Data };
            Products = searchProduct;
            SelectedProduct = null;
        }

        public void OnUpdateButtonClick()
        {
            var result = UpdateProduct().Message;
            IsPopupOpen = false;
            Products = GetProducts().Data;
            SelectedProduct = null;
            MessageBox.Show(result);
        }

        private bool ValidateData(Product? selectedProduct)
        {
            if (selectedProduct == null) return false;

            bool condition = selectedProduct.ProductName != null && selectedProduct.CategoryId != null
                    && selectedProduct.SupplierId != null && selectedProduct.UnitPrice != null && selectedProduct.QuantityPerUnit != null
                    && selectedProduct.ProductName.Length > 0 && selectedProduct.CategoryId > 0 && selectedProduct.SupplierId > 0
                    && selectedProduct.UnitPrice > 0 && selectedProduct.QuantityPerUnit.Length > 0;

            //Check for create product
            if (selectedProduct.ProductId == null || selectedProduct.ProductId <= 0)
            {
                return condition;
            }

            //Check for update product
            else
            {
                var oldEntity = ValidateProduct;
                if (oldEntity == null) return false;
                return (condition && (selectedProduct.ProductName != oldEntity.ProductName || selectedProduct.CategoryId != oldEntity.CategoryId
                                      || selectedProduct.SupplierId != oldEntity.SupplierId || selectedProduct.UnitPrice != oldEntity.UnitPrice || selectedProduct.QuantityPerUnit != oldEntity.QuantityPerUnit));
            }
        }

        private ResponseModel<List<Product>> GetProducts()
        {
            var requestModel = new RequestModel<dynamic>
            {
                Type = Int32.Parse(Configuration["RequestType:GetAll"] ?? String.Empty),
                Data = null
            };
            string? result = GetMessage(JsonConvert.SerializeObject(requestModel));
            if (result != null)
            {
                try
                {
                    var responseModel = JsonConvert.DeserializeObject<ResponseModel<List<Product>>>(result);
                    return responseModel;
                }
                catch (Exception ex)
                {
                    return new ResponseModel<List<Product>>
                    {
                        Type = -1,
                        Message = ex.Message
                    };
                }
            }
            return new ResponseModel<List<Product>>
            {
                Type = 0,
                Message = "Cannot deserialize response data"
            };
        }

        private ResponseModel<dynamic> CreateProduct()
        {
            if (!ValidateData(SelectedProduct))
            {
                return new ResponseModel<dynamic>
                {
                    Type = 0,
                    Message = "Invalid input data"
                };
            }
            var requestModel = new RequestModel<dynamic>
            {
                Type = Int32.Parse(Configuration["RequestType:Create"] ?? String.Empty),
                Data = SelectedProduct
            };
            string? result = GetMessage(JsonConvert.SerializeObject(requestModel));
            if (result != null)
            {
                try
                {
                    var responseModel = JsonConvert.DeserializeObject<ResponseModel<dynamic>>(result);
                    return responseModel;
                }
                catch (Exception ex)
                {
                    return new ResponseModel<dynamic>
                    {
                        Type = -1,
                        Message = ex.Message
                    };
                }
            }
            return new ResponseModel<dynamic>
            {
                Type = 0,
                Message = "Cannot deserialize response data"
            };
        }

        private ResponseModel<dynamic> DeleteProduct()
        {
            var requestModel = new RequestModel<dynamic>
            {
                Type = Int32.Parse(Configuration["RequestType:Delete"] ?? String.Empty),
                Data = SelectedProduct
            };
            string? result = GetMessage(JsonConvert.SerializeObject(requestModel));
            if (result != null)
            {
                try
                {
                    var responseModel = JsonConvert.DeserializeObject<ResponseModel<dynamic>>(result);
                    return responseModel;
                }
                catch (Exception ex)
                {
                    return new ResponseModel<dynamic>
                    {
                        Type = -1,
                        Message = ex.Message
                    };
                }
            }
            return new ResponseModel<dynamic>
            {
                Type = 0,
                Message = "Cannot deserialize response data"
            };
        }

        private ResponseModel<Product> SearchProduct(string? searchID)
        {
            if (searchID == null || searchID.Length <= 0)
            {
                return new ResponseModel<Product>
                {
                    Type = 0,
                    Message = "Invalid searchID"
                };
            }
            RequestModel<dynamic> requestModel;
            try
            {
                requestModel = new RequestModel<dynamic>
                {
                    Type = Int32.Parse(Configuration["RequestType:Search"] ?? String.Empty),
                    SearchId = Int32.Parse(searchID)
                };
            }
            catch(Exception e)
            {
                return new ResponseModel<Product>
                {
                    Type = -1,
                    Message = "Invalid input searchID"
                };
            }
            string? result = GetMessage(JsonConvert.SerializeObject(requestModel));
            if (result != null)
            {
                try
                {
                    var responseModel = JsonConvert.DeserializeObject<ResponseModel<Product>>(result);
                    return responseModel;
                }
                catch (Exception ex)
                {
                    return new ResponseModel<Product>
                    {
                        Type = -1,
                        Message = ex.Message
                    };
                }
            }
            return new ResponseModel<Product>
            {
                Type = 0,
                Message = "Cannot deserialize response data"
            };
        }

        private ResponseModel<dynamic> UpdateProduct()
        {
            if (!ValidateData(SelectedProduct))
            {
                return new ResponseModel<dynamic>
                {
                    Type = 0,
                    Message = "Invalid input data"
                };
            }
            var requestModel = new RequestModel<dynamic>
            {
                Type = Int32.Parse(Configuration["RequestType:Update"] ?? String.Empty),
                Data = SelectedProduct
            };
            string? result = GetMessage(JsonConvert.SerializeObject(requestModel));
            if (result != null)
            {
                try
                {
                    var responseModel = JsonConvert.DeserializeObject<ResponseModel<dynamic>>(result);
                    return responseModel;
                }
                catch (Exception ex)
                {
                    return new ResponseModel<dynamic>
                    {
                        Type = -1,
                        Message = ex.Message
                    };
                }
            }
            return new ResponseModel<dynamic>
            {
                Type = 0,
                Message = "Cannot deserialize response data"
            };
        }
    }
}
