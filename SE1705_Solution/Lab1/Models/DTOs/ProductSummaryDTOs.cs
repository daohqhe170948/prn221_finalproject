﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Models.DTOs
{
    public class ProductSummaryDTOs
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int TotalSold { get; set; }
    }
}
