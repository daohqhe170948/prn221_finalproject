﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Models.DTOs
{
    public class ProductWindowDTOs
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int SupplierID { get; set; }
        public string Supplier { get; set; }
        public decimal? UnitPrice { get; set; }
        public int TotalUnitSold { get; set; }
    }
}
