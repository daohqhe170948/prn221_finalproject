﻿using Lab1.Extensions;
using Lab1.Models;
using Lab1.Services;
using Lab1.Services.Interface;
using Lab1.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows;

namespace Lab1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IServiceProvider _serviceProvider;
        public IConfiguration Configuration { get; private set; }

        public App()
        {
            var services = new ServiceCollection();
            Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            ConfigureServices(services);
            _serviceProvider = services.BuildServiceProvider();
        }

        public IServiceProvider ServiceProvider => _serviceProvider;

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<NorthWindContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("MyCnn")));

            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ISupplierService, SupplierService>();

            services.AddSingleton<ProductWindowVM>();
            services.AddSingleton<SummaryChartWindowVM>();
        }
    }
}
