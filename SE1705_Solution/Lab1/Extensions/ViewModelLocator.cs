﻿using Lab1.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab1.Extensions
{
    public class ViewModelLocator
    {
        private readonly IServiceProvider _serviceProvider;

        public ViewModelLocator()
        {
            _serviceProvider = ((App)Application.Current).ServiceProvider;
        }

        public ProductWindowVM ProductWindowVM => _serviceProvider.GetService<ProductWindowVM>();
        public SummaryChartWindowVM SummaryChartWindowVM => _serviceProvider.GetService<SummaryChartWindowVM>();
    }
}
