﻿using Lab1.Models;
using Lab1.Models.DTOs;
using Microsoft.Win32;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab1.Extensions
{
    public class ExcelExtension
    {
        public static void ExportProductsToExcel(List<ProductWindowDTOs> productList)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Excel Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*",
                FileName = "Products.xlsx"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                string filePath = saveFileDialog.FileName;

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Products");

                    worksheet.Cells[1, 1].Value = "ID";
                    worksheet.Cells[1, 2].Value = "Name";
                    worksheet.Cells[1, 3].Value = "Category";
                    worksheet.Cells[1, 4].Value = "Supplier";
                    worksheet.Cells[1, 5].Value = "UnitPrice";

                    int row = 2;
                    foreach (var product in productList)
                    {
                        worksheet.Cells[row, 1].Value = product.ProductID;
                        worksheet.Cells[row, 2].Value = product.ProductName;
                        worksheet.Cells[row, 3].Value = product.CategoryName;
                        worksheet.Cells[row, 4].Value = product.Supplier;
                        worksheet.Cells[row, 5].Value = product.UnitPrice;

                        row++;
                    }

                    File.WriteAllBytes(filePath, package.GetAsByteArray());
                }
            }
        }
    }
}
