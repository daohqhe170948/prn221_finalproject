﻿using Lab1.Models;
using Lab1.Models.DTOs;
using Lab1.Services.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.ViewModels
{
    public class ProductService : IProductService
    {
        private readonly NorthWindContext _northWindContext;

        public ProductService(NorthWindContext northWindContext)
        {
            this._northWindContext = northWindContext;
        }

        public async Task<List<ProductWindowDTOs>> GetProducts()
        {
            return await (from product in _northWindContext.Products
                          join c in _northWindContext.Categories
                          on product.CategoryId equals c.CategoryId
                          join s in _northWindContext.Suppliers
                          on product.SupplierId equals s.SupplierId
                          select new ProductWindowDTOs
                          {
                              ProductID = product.ProductId,
                              ProductName = product.ProductName,
                              CategoryID = c.CategoryId,
                              CategoryName = c.CategoryName,
                              SupplierID = s.SupplierId,
                              Supplier = s.ContactName,
                              UnitPrice = product.UnitPrice,
                          }).ToListAsync();
        }

        public List<ProductWindowDTOs> GetProductsByCategoryId(int categoryId)
        {
            return (from product in _northWindContext.Products
                    join order in _northWindContext.OrderDetails on product.ProductId equals order.ProductId
                    join c in _northWindContext.Categories
                      on product.CategoryId equals c.CategoryId where c.CategoryId == categoryId
                      join s in _northWindContext.Suppliers
                      on product.SupplierId equals s.SupplierId
                    group order by new { product.ProductId, product.ProductName, c.CategoryId, c.CategoryName, s.SupplierId, s.ContactName, product.UnitPrice } into grouped
                    select new ProductWindowDTOs
                      {
                          ProductID = grouped.Key.ProductId,
                          ProductName = grouped.Key.ProductName,
                          CategoryID = grouped.Key.CategoryId,
                          CategoryName = grouped.Key.CategoryName,
                          SupplierID = grouped.Key.SupplierId,
                          Supplier = grouped.Key.ContactName,
                          UnitPrice = grouped.Key.UnitPrice,
                          TotalUnitSold = grouped.Sum(o => o.Quantity)
                    }).ToList();
        }

        public ObservableCollection<ProductSummaryDTOs> GetProductsSummaryByCategoryId(int categoryId)
        {
            var list = (from product in _northWindContext.Products where product.CategoryId == categoryId
                    join order in _northWindContext.OrderDetails on product.ProductId equals order.ProductId
                    into productOrderDetailGroup
                    from order in productOrderDetailGroup.DefaultIfEmpty()
                    group order by new { product.ProductId, product.ProductName } into grouped
                    select new ProductSummaryDTOs
                    {
                        ProductID = grouped.Key.ProductId,
                        ProductName = grouped.Key.ProductName,
                        TotalSold = grouped.Sum(o => o.Quantity)
                    }
                    ).OrderBy(p => p.ProductID).ToList();
            return new ObservableCollection<ProductSummaryDTOs>(list);
        }

        public bool? CreateProduct(ProductWindowDTOs? product)
        {
            if (product == null) return null;
            var currentProduct = _northWindContext.Products.AsNoTracking().FirstOrDefault(p => p.ProductId == product.ProductID);
            if (currentProduct == null) return null;
            if (product.ProductName == currentProduct.ProductName 
                && product.CategoryID == currentProduct.CategoryId
                && product.SupplierID == currentProduct.SupplierId
                && product.UnitPrice == currentProduct.UnitPrice) return false;
            Product newProduct = new Product
            {
                ProductName = product.ProductName,
                CategoryId = product.CategoryID,
                SupplierId = product.SupplierID,
                UnitPrice = product.UnitPrice,
            };
            _northWindContext.Add(newProduct);
            _northWindContext.SaveChanges();
            return true;
        }

        public bool? UpdateProduct(ProductWindowDTOs? product)
        {
            if(product == null) return null;
            var currentProduct = _northWindContext.Products.FirstOrDefault(p => p.ProductId == product.ProductID);
            if (currentProduct == null) return null;
            if (product.ProductName == currentProduct.ProductName
                && product.CategoryID == currentProduct.CategoryId
                && product.SupplierID == currentProduct.SupplierId
                && product.UnitPrice == currentProduct.UnitPrice) return false;

            currentProduct.ProductName = product.ProductName;
            currentProduct.CategoryId = product.CategoryID;
            currentProduct.SupplierId = product.SupplierID;
            currentProduct.UnitPrice = product.UnitPrice;
            _northWindContext.SaveChanges();
            return true;
        }

        public bool? DeleteProduct(int? productId)
        {
            if(productId == null) return null;
            var currentProduct = _northWindContext.Products.FirstOrDefault(p => p.ProductId == productId);
            if (currentProduct == null) return null;
            var isHavingOrder = _northWindContext.OrderDetails.Any(o => o.ProductId == productId);
            if(isHavingOrder) return false;
            _northWindContext.Products.Remove(currentProduct);
            _northWindContext.SaveChanges();
            return true;
        }
    }
}
