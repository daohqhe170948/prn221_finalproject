﻿using Lab1.Models;
using Lab1.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly NorthWindContext _northWindContext;

        public CategoryService(NorthWindContext northWindContext)
        {
            this._northWindContext = northWindContext;
        }

        public List<Category> GetCategories()
        {
            return _northWindContext.Categories.ToList();
        }
    }
}
