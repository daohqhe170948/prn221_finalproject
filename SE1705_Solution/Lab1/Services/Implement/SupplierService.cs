﻿using Lab1.Models;
using Lab1.Models.DTOs;
using Lab1.Services.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.ViewModels
{
    public class SupplierService : ISupplierService
    {
        private readonly NorthWindContext _northWindContext;

        public SupplierService(NorthWindContext northWindContext)
        {
            this._northWindContext = northWindContext;
        }

        public Supplier? GetSupplierById(int? supplierId)
        {
            return _northWindContext.Suppliers.FirstOrDefault(s => s.SupplierId == supplierId);
        }

        public List<Supplier> GetSuppliers()
        {
            return _northWindContext.Suppliers.ToList();
        }
    }
}
