﻿using Lab1.Models;
using Lab1.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Services.Interface
{
    public interface IProductService
    {
        Task<List<ProductWindowDTOs>> GetProducts();
        List<ProductWindowDTOs> GetProductsByCategoryId(int categoryId);
        ObservableCollection<ProductSummaryDTOs> GetProductsSummaryByCategoryId(int categoryId);
        //false khi khong thay doi du lieu nao
        bool? CreateProduct(ProductWindowDTOs? product);
        bool? UpdateProduct(ProductWindowDTOs? product);
        //null khi productID null, false khi con order khong duoc xoa
        bool? DeleteProduct(int? productId);

    }
}
