﻿using Lab1.Models;
using Lab1.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Services.Interface
{
    public interface ISupplierService
    {
        Supplier? GetSupplierById(int? supplierId);
        List<Supplier> GetSuppliers();
    }
}
