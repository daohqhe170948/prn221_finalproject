﻿using Lab1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Services.Interface
{
    public interface ICategoryService
    {
        List<Category> GetCategories();
    }
}
