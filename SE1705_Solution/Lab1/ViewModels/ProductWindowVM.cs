﻿using GalaSoft.MvvmLight.Command;
using Lab1.Models;
using Lab1.Models.DTOs;
using Lab1.Services;
using Lab1.Services.Interface;
using LiveCharts.Wpf;
using LiveCharts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Lab1.Extensions;

namespace Lab1.ViewModels
{
    public class ProductWindowVM : BaseVM
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly ISupplierService _supplierService;

        private bool _isPopupOpen;

        private List<Category> _categories;
        private List<Supplier> _suplliers; 
        private Category _selectedCategory;
        private Category _selectedEditCategory;
        private ProductWindowDTOs? _selectedProduct;
        private Supplier _selectedSupplier;
        private List<ProductWindowDTOs> _products;

        private RelayCommand _exitTabCommand;
        private RelayCommand _addProductCommand;
        private RelayCommand _deleteProductCommand;
        private RelayCommand _editProductCommand;
        private RelayCommand _showSummaryCommand;
        private RelayCommand _exportDataCommand;
        private RelayCommand _createProductCommand;

        #region Getter, Setter
        public bool IsPopupOpen
        {
            get => _isPopupOpen;
            set
            {
                _isPopupOpen = value;
                InvokeChanged(nameof(IsPopupOpen));
            }
        }
        public List<Category> Categories
        {
            get => _categories;
            set
            {
                _categories = value;
                InvokeChanged(nameof(Categories));
            }
        }

        public List<Supplier> Suppliers
        {
            get => _suplliers;
            set => _suplliers = value;
        }

        public Category? SelectedCategory
        {
            get => _selectedCategory;
            set
            {
                _selectedCategory = value;
                int newCategoryId = SelectedCategory.CategoryId;
                Products = _productService.GetProductsByCategoryId(newCategoryId);
                InvokeChanged(nameof(SelectedCategory));
            }
        }

        public Category? SelectedEditCategory
        {
            get => _selectedEditCategory;
            set
            {
                _selectedEditCategory = value;
                InvokeChanged(nameof(SelectedEditCategory));
            }
        }

        public ProductWindowDTOs? SelectedProduct
        {
            get => _selectedProduct;
            set
            {
                _selectedProduct = value;
                if(value != null) IsPopupOpen = true;

                //Change Selected Supplier
                SelectedSupplier = _supplierService.GetSupplierById(SelectedProduct?.SupplierID);
                SelectedEditCategory = SelectedCategory;
                InvokeChanged(nameof(SelectedProduct));
            }
        }

        public Supplier? SelectedSupplier
        {
            get => _selectedSupplier;
            set
            {
                _selectedSupplier = value;
                InvokeChanged(nameof(SelectedSupplier));
            }
        }

        public List<ProductWindowDTOs> Products
        {
            get => _products;
            set
            {
                _products = value;
                InvokeChanged(nameof(Products));
            }
        }

        public RelayCommand ExitTabCommand
        {
            get => _exitTabCommand;
            set => _exitTabCommand = value;
        }

        public RelayCommand AddProductCommand
        {
            get => _addProductCommand;
            set => _addProductCommand = value;
        }

        public RelayCommand DeleteProductCommand
        {
            get => _deleteProductCommand;
            set => _deleteProductCommand = value;
        } 

        public RelayCommand EditProductCommand
        {
            get => _editProductCommand;
            set => _editProductCommand = value;
        }

        public RelayCommand ShowSummaryCommand
        {
            get => _showSummaryCommand;
            set => _showSummaryCommand = value;
        }

        public RelayCommand ExportDataCommand
        {
            get => _exportDataCommand;
            set => _exportDataCommand = value;
        }

        public RelayCommand CreateProductCommand
        {
            get => _createProductCommand;
            set => _createProductCommand = value;
        }
        #endregion

        #region Constructor
        public ProductWindowVM(ICategoryService categoryService, IProductService productService, ISupplierService supplierService)
        {
            _categoryService = categoryService;
            _productService = productService;
            _supplierService = supplierService;

            Categories = _categoryService.GetCategories();
            Suppliers = _supplierService.GetSuppliers();

            SelectedCategory = Categories.FirstOrDefault();
            Products = _productService.GetProductsByCategoryId(SelectedCategory.CategoryId);
            SelectedProduct = null;
            IsPopupOpen = false;

            ExitTabCommand = new RelayCommand(OnExitButtonClick);
            AddProductCommand = new RelayCommand(OnAddButtonClick);
            DeleteProductCommand = new RelayCommand(OnDeleteButtonClick);
            EditProductCommand = new RelayCommand(OnEditButtonClick);
            ShowSummaryCommand = new RelayCommand(OnShowSummaryButtonClick);
            ExportDataCommand = new RelayCommand(OnExportDataButtonClick);
            CreateProductCommand = new RelayCommand(OnCreateProductButtonClick);
        }
        #endregion

        #region RelayCommand
        public void OnExitButtonClick()
        {
            IsPopupOpen = false;
            SelectedProduct = null;
        }
        
        public void OnAddButtonClick()
        {
            SelectedProduct.CategoryID = SelectedEditCategory.CategoryId;
            SelectedProduct.SupplierID = SelectedSupplier.SupplierId;
            var result = _productService.CreateProduct(SelectedProduct);
            if (result == null) MessageBox.Show("Khong ton tai product");
            else if (result == false) MessageBox.Show("Thong tin product moi bi trung");
            else
            {
                IsPopupOpen = false;
                SelectedProduct = null;
                Products = _productService.GetProductsByCategoryId(SelectedCategory.CategoryId);
            }
        }

        public void OnDeleteButtonClick()
        {
            var result = _productService.DeleteProduct(SelectedProduct?.ProductID);
            if (result == null) MessageBox.Show("Khong ton tai product");
            else if (result == false) MessageBox.Show("Khong the xoa product do dang co order");
            else
            {
                IsPopupOpen = false;
                SelectedProduct = null;
                Products = _productService.GetProductsByCategoryId(SelectedCategory.CategoryId);
            }
        }

        public void OnEditButtonClick()
        {
            SelectedProduct.CategoryID = SelectedEditCategory.CategoryId;
            SelectedProduct.SupplierID = SelectedSupplier.SupplierId;
            var result = _productService.UpdateProduct(SelectedProduct);
            if (result == null) MessageBox.Show("Khong ton tai product");
            else if (result == false) MessageBox.Show("Khong co gi de update");
            else
            {
                IsPopupOpen = false;
                SelectedProduct = null;
                Products = _productService.GetProductsByCategoryId(SelectedCategory.CategoryId);
            }
        }

        public void OnShowSummaryButtonClick()
        {
            var x = ((App)Application.Current).ServiceProvider.GetService<SummaryChartWindowVM>();
            x.ProductData = _productService.GetProductsSummaryByCategoryId(this.SelectedCategory.CategoryId);
            x.SeriesCollection = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Product Total Sold",
                    Values = new ChartValues<int>(x.ProductData.Select(p => p.TotalSold)),
                    DataLabels = true,
                }
            };
            x.Labels = x.ProductData.Select(p => p.ProductName).ToList();
            x.CurrentColumnSelected = x.ProductData.First();
            SummaryChartWindow newWindow = new SummaryChartWindow();
            newWindow.Show();
        }

        public async void OnExportDataButtonClick()
        {
            var products = await _productService.GetProducts();
            ExcelExtension.ExportProductsToExcel(products);
        }

        public void OnCreateProductButtonClick()
        {
            SelectedProduct = new ProductWindowDTOs();
        }
        #endregion
    }
}
