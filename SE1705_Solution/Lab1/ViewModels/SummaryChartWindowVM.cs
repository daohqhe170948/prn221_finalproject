﻿using Lab1.Models;
using Lab1.Models.DTOs;
using Lab1.Services.Interface;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab1.ViewModels
{
    public class SummaryChartWindowVM : BaseVM
    {
        private ObservableCollection<ProductSummaryDTOs> _productData;

        public SeriesCollection SeriesCollection { get; set; }

        public List<string> _labels;

        private ProductSummaryDTOs? _currentColumnSelected;

        public ProductSummaryDTOs? CurrentColumnSelected 
        {
            get => _currentColumnSelected;
            set
            {
                _currentColumnSelected = value;
                InvokeChanged(nameof(CurrentColumnSelected));
            }
        }

        public ObservableCollection<ProductSummaryDTOs> ProductData
        {
            get => _productData; 
            set
            {
                _productData = value;
                InvokeChanged(nameof(ProductData));
            }
        }

        public List<string> Labels
        {
            get => _labels;
            set
            {
                _labels = value;
                InvokeChanged(nameof(Labels));
            }
        }

        public SummaryChartWindowVM()
        {
        }
    }
}
