using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPage.Models;

namespace RazorPage.Pages.Employee
{
    public class DetailModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }
        
        public Models.Employee? emp { get; set; }
        public NorthwindContext _dbContext;

        public DetailModel(NorthwindContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void OnGet()
        {
            emp = _dbContext.Employees.FirstOrDefault(e => e.EmployeeId == Id);
        }
    }
}
