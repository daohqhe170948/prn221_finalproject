using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPage.Models;

namespace RazorPage.Pages.Employee
{
    public class ListModel : PageModel
    {
        public NorthwindContext _dbContext;
        public List<RazorPage.Models.Employee> Employees { get; set; }

        public ListModel(NorthwindContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void OnGet()
        {
            Employees = _dbContext.Employees.ToList();
        }
    }
}
