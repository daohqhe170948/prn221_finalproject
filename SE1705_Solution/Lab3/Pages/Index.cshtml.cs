﻿using Lab3.Models;
using Lab3.Models.OtherModel;
using Lab3.Repos.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Lab3.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<IndexModel> _logger;
        private readonly IProductRepository _productRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICartRepository _cartRepository;
        private readonly ICategoryRepository _categoryRepository;

        private static int PageSize { get; set; }
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }

        [BindProperty(SupportsGet = true, Name = "category")]
        public int SelectedCategoryId { get; set; }

        [BindProperty(SupportsGet = true)]
        public int PageNumber { get; set; }
        public int PageCount { get; set; }

        [BindProperty(SupportsGet = true)]
        public string SearchKey { get; set; }

        public Cart MyCart { get; set; }
        
        public IndexModel(ILogger<IndexModel> logger, IProductRepository productRepository, IConfiguration configuration, ICartRepository cartRepository, ICategoryRepository categoryRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
            _configuration = configuration;
            _cartRepository = cartRepository;
            _categoryRepository = categoryRepository;
            PageSize = Int32.Parse(_configuration["Paging:PageSize"] ?? String.Empty);
        }

        public void OnGet()
        {
            if (SearchKey == null) SearchKey = String.Empty;
            if(SelectedCategoryId > 0)
            {
                Products = Paging(_productRepository.Search(p => p.CategoryId == SelectedCategoryId && p.ProductName.Contains(SearchKey)));
            }
            else Products = Paging(_productRepository.SearchByName(SearchKey));
            Categories = _categoryRepository.GetAll();
        }

        public void OnPost()
        {
            if (SearchKey == null) SearchKey = String.Empty;
            if (SelectedCategoryId > 0)
            {
                Products = Paging(_productRepository.Search(p => p.CategoryId == SelectedCategoryId && p.ProductName.Contains(SearchKey)));
            }
            else Products = Paging(_productRepository.SearchByName(SearchKey));
            Categories = _categoryRepository.GetAll();
        }


        private List<Product> Paging(List<Product> originList)
        {
            if (PageNumber < 1) PageNumber = 1;
            var count = originList.Count;
            PageCount = (int)Math.Ceiling((decimal)count / PageSize);
            if (PageNumber > PageCount) PageNumber = PageCount;
            return originList.Skip((PageNumber - 1) * PageSize).Take(PageSize).ToList();
        }

        
    }
}