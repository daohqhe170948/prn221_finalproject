﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var connection = new signalR.HubConnectionBuilder().withUrl("/cartHub").build();

connection.start().then(function () {
    console.log("Connected");
    //connection.invoke("GetMyCart").catch(function (err) {
    //    return console.error(err.toString());
    //});;
}
).catch(function (err) {
    return console.error(err.toString());
});


connection.on("CartUpdated", cart => {
    console.log(cart);
    GenerateCart(cart);
    AddNewEventListener();
});

connection.on("OrderCreated", function (result, cart) {
    alert(result);
    GenerateCart(cart);
    AddNewEventListener();
});

connection.on("PageLoad", result => {
    console.log(result);
});

document.querySelectorAll("a.add_to_cart_button").forEach(function (button) {
    button.addEventListener("click", function (event) {
        const productId = button.getAttribute("data-ProductId");
        connection.invoke("AddToCart", productId).catch(function (err) {
            return console.error(err.toString());
        });;
        event.preventDefault();
    });
})
function AddNewEventListener() {
    document.querySelectorAll("a.remove_button").forEach(function (button) {
        button.addEventListener("click", function (event) {
            const productId = button.getAttribute("data-ProductId");
            connection.invoke("RemoveFromCart", productId).catch(function (err) {
                return console.error(err.toString());
            });;
            event.preventDefault();
        });
    })

    document.querySelectorAll("a.clear_button").forEach(function (button) {
        button.addEventListener("click", function (event) {
            const productId = button.getAttribute("data-ProductId");
            connection.invoke("ClearFromCart", productId).catch(function (err) {
                return console.error(err.toString());
            });;
            event.preventDefault();
        });
    })
}

document.getElementById("create_order_btn").addEventListener("click", function (event) {
    connection.invoke("CreateOrder").catch(function (err) {
        return console.error(err.toString());
    });;
    event.preventDefault();
});

function GenerateCart(cart) {
    const cartArea = document.getElementById("cart_area");
    cartArea.innerHTML = "";

    if (cart.items && cart.items.length > 0) {
        const cartTable = document.createElement("table");
        document.getElementById("create_order_btn").disabled = false;

        const tableHeader = document.createElement("tr");
        const idHead = document.createElement("th");
        idHead.textContent = "ID";
        tableHeader.appendChild(idHead);

        const nameHead = document.createElement("th");
        nameHead.textContent = "Name";
        tableHeader.appendChild(nameHead);

        const priceHead = document.createElement("th");
        priceHead.textContent = "UnitPrice";
        tableHeader.appendChild(priceHead);

        const quantityHead = document.createElement("th");
        quantityHead.textContent = "Quantity";
        tableHeader.appendChild(quantityHead);

        cartTable.appendChild(tableHeader);

        // Iterate over each item in the cart and append it to the table
        cart.items.forEach(item => {
            console.log(item)
            const row = document.createElement("tr");

            const idCell = document.createElement("td");
            idCell.textContent = item.item.productId;
            row.appendChild(idCell);

            const nameCell = document.createElement("td");
            nameCell.textContent = item.item.productName;
            row.appendChild(nameCell);

            const priceCell = document.createElement("td");
            priceCell.textContent = item.item.unitPrice;
            row.appendChild(priceCell);

            const quantityCell = document.createElement("td");
            quantityCell.textContent = item.quantity;
            row.appendChild(quantityCell);

            const decreaseCell = document.createElement("td");
            const clearCell = document.createElement("td");

            const decreaseButton = document.createElement("a");
            decreaseButton.className = "remove_button";
            decreaseButton.setAttribute("data-ProductId", item.item.productId);
            decreaseButton.textContent = "Remove";
            decreaseCell.appendChild(decreaseButton);

            const clearButton = document.createElement("a");
            clearButton.className = "clear_button";
            clearButton.setAttribute("data-ProductId", item.item.productId);
            clearButton.textContent = "Clear";
            clearCell.appendChild(clearButton);

            row.appendChild(decreaseCell);
            row.appendChild(clearCell);

            cartTable.appendChild(row);
        });
        cartArea.appendChild(cartTable);

        cartArea.appendChild(document.createElement("br"));
        const totalMoney = document.createElement("p");
        totalMoney.textContent = "Total Cost: ";

        const cost = document.createElement("span");
        cost.style = "color: red;";
        cost.textContent = cart.totalCost;

        totalMoney.appendChild(cost);
        cartArea.appendChild(totalMoney);
    }
    else {
        document.getElementById("create_order_btn").disabled = true;
        const row = document.createElement("p");
        row.textContent = "Your cart is empty";
        cartArea.appendChild(row);
    }
}
