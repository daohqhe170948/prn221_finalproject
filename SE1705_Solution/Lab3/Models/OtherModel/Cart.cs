﻿namespace Lab3.Models.OtherModel
{
    public record Cart
    {
        public List<CartItem> Items { get; set;}
        public decimal TotalCost { get; set;}

        public Cart()
        {
            Items = new List<CartItem>();
        }

        public decimal GetTotalCost()
        {
            decimal totalCost = 0;
            foreach (var item in Items)
            {
                totalCost += (item.Item.UnitPrice ?? 0) * item.Quantity;
            }
            return totalCost;
        }
    }

    public record CartItem
    {
        public Product Item { get; set; }
        public int Quantity { get; set; }
    }
}
