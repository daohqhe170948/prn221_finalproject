﻿namespace Lab3.Models.OtherModel
{
    public class HubResponse<T> where T : class
    {
        public T Data { get; set; }
        public string Message { get; set; }
    }
}
