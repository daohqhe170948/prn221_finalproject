﻿using Lab3.Models;
using Lab3.Repos.Interfaces;

namespace Lab3.Repos.Implements
{
    public class OrderRepository : DataRepository, IOrderRepository
    {
        public OrderRepository(NorthwindContext northwindContext) : base(northwindContext) { }

        public bool CreateOrder(Order order)
        {
            DBContext.Orders.Add(order);
            var result = DBContext.SaveChanges();
            return result > 0;
        }

        public List<Order> GetAll()
        {
            return DBContext.Orders.ToList();
        }

        public Order? GetById(int id)
        {
            return DBContext.Orders.FirstOrDefault(o => o.OrderId == id);
        }
    }
}
