﻿using Lab3.Models;
using Lab3.Repos.Interfaces;

namespace Lab3.Repos.Implements
{
    public class DataRepository
    {
        public NorthwindContext DBContext;

        public DataRepository(NorthwindContext dBContext)
        {
            DBContext = dBContext;
        }
    }
}
