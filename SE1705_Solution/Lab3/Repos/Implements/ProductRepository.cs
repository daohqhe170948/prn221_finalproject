﻿using Lab3.Models;
using Lab3.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Lab3.Repos.Implements
{
    public class ProductRepository : DataRepository, IProductRepository
    {
        public ProductRepository(NorthwindContext northwindContext) : base(northwindContext) { }

        public List<Product> GetAll()
        {
            return DBContext.Products.ToList();
        }

        public Product? GetById(int id)
        {
            return DBContext.Products.FirstOrDefault(p => p.ProductId == id);
        }

        public List<Product> SearchByName(string name)
        {
            return DBContext.Products.Where(p => p.ProductName.Contains(name)).Include(p => p.Category).ToList();
        }
        public List<Product> Search(Expression<Func<Product, bool>> expression)
        {
            return DBContext.Products.Where(expression).Include(p => p.Category).ToList();
        }
    }
}
