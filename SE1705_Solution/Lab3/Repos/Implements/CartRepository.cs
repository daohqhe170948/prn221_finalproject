﻿using Lab3.Models.OtherModel;
using Lab3.Repos.Interfaces;

namespace Lab3.Repos.Implements
{
    public class CartRepository : ICartRepository
    {
        private readonly Dictionary<string, Cart> _cartRepos = new Dictionary<string, Cart>();

        public void AddToCart(string connectionId, Cart cart)
        {
            _cartRepos.Add(connectionId, cart);
        }
        public Cart? GetCart(string connectionId)
        {
            _cartRepos.TryGetValue(connectionId, out var cart);
            return cart;
        }

        public void ModifyCart(string connectionId, Cart newCart) 
        {
            _cartRepos[connectionId] = newCart;
        }

        public bool CartExists(string connectionId)
        {
            return _cartRepos.ContainsKey(connectionId);
        }
    }
}
