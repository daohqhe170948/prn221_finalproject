﻿using Lab3.Models;
using Lab3.Repos.Interfaces;

namespace Lab3.Repos.Implements
{
    public class CategoryRepository : DataRepository, ICategoryRepository
    {
        public CategoryRepository(NorthwindContext northwindContext) : base(northwindContext) { }

        public List<Category> GetAll()
        {
            return DBContext.Categories.ToList();
        }

        public Category? GetById(int id)
        {
            return DBContext.Categories.FirstOrDefault(c => c.CategoryId == id);
        }
    }
}
