﻿using Lab3.Models.OtherModel;

namespace Lab3.Repos.Interfaces
{
    public interface ICartRepository
    {
        void AddToCart(string connectionId, Cart cart);
        Cart? GetCart(string connectionId);
        void ModifyCart(string connectionId, Cart newCart);
        bool CartExists(string connectionId);
    }
}
