﻿using Lab3.Models;

namespace Lab3.Repos.Interfaces
{
    public interface IOrderRepository : IDataRepository<Order>
    {
        bool CreateOrder(Order order);
    }
}
