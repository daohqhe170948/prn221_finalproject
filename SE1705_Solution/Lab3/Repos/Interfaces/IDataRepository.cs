﻿namespace Lab3.Repos.Interfaces
{
    public interface IDataRepository<T> where T : class
    {
        public List<T> GetAll();
        public T? GetById(int id);
    }
}
