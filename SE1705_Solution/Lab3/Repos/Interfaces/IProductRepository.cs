﻿using Lab3.Models;
using System.Linq.Expressions;

namespace Lab3.Repos.Interfaces
{
    public interface IProductRepository : IDataRepository<Product>
    {
        public List<Product> SearchByName(string name);
        public List<Product> Search(Expression<Func<Product, bool>> expression);
    }
}
