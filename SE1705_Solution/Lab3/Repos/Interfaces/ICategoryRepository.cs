﻿using Lab3.Models;

namespace Lab3.Repos.Interfaces
{
    public interface ICategoryRepository : IDataRepository<Category>
    {
    }
}
