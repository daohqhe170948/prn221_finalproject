﻿using Lab3.Models;
using Lab3.Models.OtherModel;
using Lab3.Repos.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Lab3.Hubs
{
    public class CartHub : Hub
    {
        private readonly IProductRepository _productRepository;
        private readonly ICartRepository _cartRepository;
        private readonly IOrderRepository _orderRepository;
        private Cart? MyCart { get; set; }

        public CartHub(IProductRepository productRepository, ICartRepository cartRepository, IOrderRepository orderRepository)
        {
            _productRepository = productRepository;
            _cartRepository = cartRepository;
            _orderRepository = orderRepository;
        }

        //public async Task GetMyCart()
        //{
        //    MyCart = GetCart();
        //    await Clients.Caller.SendAsync("PageLoad", Context.ConnectionId);
        //}

        public async Task AddToCart(string productID)
        {
            try
            {
                int productId = Int32.Parse(productID);
                MyCart = GetCart();
                if (MyCart.Items.Exists(item => item.Item.ProductId == productId))
                {
                    var index = MyCart.Items.FindIndex(item => item.Item.ProductId == productId);
                    MyCart.Items[index].Quantity += 1;
                }
                else
                {
                    var product = _productRepository.GetById(productId);
                    if (product != null)
                    {
                        MyCart.Items.Add(new CartItem { Item = product, Quantity = 1 });
                    }
                }
                _cartRepository.ModifyCart(Context.ConnectionId, MyCart);
                MyCart.TotalCost = MyCart.GetTotalCost();
                await Clients.Caller.SendAsync("CartUpdated", MyCart);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync("CartUpdated", ex.Message);
            }
        }

        public async Task RemoveFromCart(string productID)
        {
            try
            {
                int productId = Int32.Parse(productID);
                MyCart = GetCart();
                var index = MyCart.Items.FindIndex(p => p.Item.ProductId == productId);
                MyCart.Items[index].Quantity -= 1;
                if (MyCart.Items[index].Quantity == 0) MyCart.Items.RemoveAt(index);
                _cartRepository.ModifyCart(Context.ConnectionId, MyCart);
                MyCart.TotalCost = MyCart.GetTotalCost();
                await Clients.Caller.SendAsync("CartUpdated", MyCart);
            }
            catch(Exception ex)
            {
                await Clients.Caller.SendAsync("CartUpdated", ex.Message);
            }
        }

        public async Task ClearFromCart(string productID)
        {
            try
            {
                int productId = Int32.Parse(productID);
                MyCart = GetCart();
                var product = MyCart.Items.First(p => p.Item.ProductId == productId);
                MyCart.Items.Remove(product);
                _cartRepository.ModifyCart(Context.ConnectionId, MyCart);
                MyCart.TotalCost = MyCart.GetTotalCost();
                await Clients.Caller.SendAsync("CartUpdated", MyCart);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync("CartUpdated", ex.Message);
            }
        }
    

        private Cart? GetCart()
        {
            var connectionId = Context.ConnectionId;
            if (!_cartRepository.CartExists(connectionId))
            {
                var cart = new Cart();
                _cartRepository.AddToCart(connectionId, cart);
                return cart;
            }
            return _cartRepository.GetCart(connectionId);
        }

        public async Task CreateOrder()
        {
            MyCart = GetCart();
            var newOrder = new Order { };

            foreach(var item in MyCart.Items)
            {
                OrderDetail newOrderDetail = new OrderDetail
                {
                    Order = newOrder,
                    ProductId = item.Item.ProductId,
                    UnitPrice = item.Item.UnitPrice ?? 0,
                    Quantity = (short)item.Quantity
                };
                newOrder.OrderDetails.Add(newOrderDetail);
            }
            var result = _orderRepository.CreateOrder(newOrder);
            MyCart = new Cart();
            _cartRepository.ModifyCart(Context.ConnectionId, MyCart);
            await Clients.Caller.SendAsync("OrderCreated", result?"Created new order":"Failed to create new order", MyCart);
        }
    }
}
