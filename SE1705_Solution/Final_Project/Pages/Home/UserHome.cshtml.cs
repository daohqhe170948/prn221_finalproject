using Final_Project.Models;
using Final_Project.Repos.Implement;
using Final_Project.Repos.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Final_Project.Pages.Home
{
    [Authorize(Policy = "Student")]
    public class UserHomeModel : PageModel
    {
        private readonly IUserRepository _userRepository;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ISession _session;
        private readonly IConfiguration _configuration;
        private readonly ISlotRepository _slotRepository;
        private readonly ISlotDetailRepository _slotDetailRepository;

        public Dictionary<int, List<Slot>> Schedule { get; set; } = new Dictionary<int, List<Slot>>();
        public User? CurrentUser { get; set; }

        public UserHomeModel(IUserRepository userRepository, IHttpContextAccessor contextAccessor, IConfiguration configuration, ISlotRepository slotRepository, ISlotDetailRepository slotDetailRepository)
        {
            _userRepository = userRepository;
            _contextAccessor = contextAccessor;
            _session = _contextAccessor.HttpContext.Session;
            _configuration = configuration;
            _slotRepository = slotRepository;
            _slotDetailRepository = slotDetailRepository;
        }

        public IActionResult OnGet()
        {
            var id = Int32.Parse(User.FindFirst("ID")?.Value ?? "0");
            CurrentUser = _userRepository.Get(u => u.Id == id);
            var startDate = DateTime.Parse(_configuration["SemesterPeriod:StartDate"]);
            List<Slot> slots = new List<Slot>();
            var data = _slotRepository.Gets(s => s.SlotDate >= startDate && s.SlotDate <= startDate.AddDays(76));
            foreach(var slot in data)
            {
                if(_slotDetailRepository.Get(s => s.SlotId == slot.Id && s.StudentId == (CurrentUser == null ? 0 : CurrentUser.Id)) != null)
                {
                    slots.Add(slot);
                }
            }
            GetSchedule(slots, Schedule);

            return Page();
        }

        public void OnPost()
        {
        }

        private void GetSchedule(List<Slot> data, Dictionary<int, List<Slot>> schedule)
        {
            var mondays = new List<Slot>();
            var tuesdays = new List<Slot>();
            var wednesdays = new List<Slot>();
            var thursdays = new List<Slot>();
            var fridays = new List<Slot>();
            var saturdays = new List<Slot>();
            var sundays = new List<Slot>();

            foreach (var item in data)
            {
                switch (item.SlotDate.DayOfWeek.ToString())
                {
                    case "Monday":
                        {
                            mondays.Add(item);
                            break;
                        }
                    case "Tuesday":
                        {
                            tuesdays.Add(item);
                            break;
                        }
                    case "Wednesday":
                        {
                            wednesdays.Add(item);
                            break;
                        }
                    case "Thursday":
                        {
                            thursdays.Add(item);
                            break;
                        }
                    case "Friday":
                        {
                            fridays.Add(item);
                            break;
                        }
                    case "Saturday":
                        {
                            saturdays.Add(item);
                            break;
                        }
                    case "Sunday":
                        {
                            sundays.Add(item);
                            break;
                        }
                }
            }

            schedule.Add(2, mondays);
            schedule.Add(3, tuesdays);
            schedule.Add(4, wednesdays);
            schedule.Add(5, thursdays);
            schedule.Add(6, fridays);
            schedule.Add(7, saturdays);
            schedule.Add(8, sundays);
        }
    }
}
