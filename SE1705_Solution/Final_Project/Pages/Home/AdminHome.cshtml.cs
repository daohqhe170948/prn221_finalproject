using Final_Project.Models;
using Final_Project.Repos.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Final_Project.Pages.Home
{
    [Authorize(Policy = "Admin")]
    public class AdminHomeModel : PageModel
    {
        private readonly IUserRepository _userRepository;

        [BindProperty(SupportsGet = true, Name = "role")]
        public int Role { get; set; }

        public string ListTitle;
        public List<User> Users { get; set; }
        public User? CurrentUser { get; set; }

        public AdminHomeModel(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public void OnGet()
        {
            ListTitle = Role == 0 ? "List Students" : "List Mentors";
            Users = _userRepository.Gets(u => u.Role == Role);
        }

    }
}
