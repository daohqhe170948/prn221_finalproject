using Final_Project.Repos.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Final_Project.Pages.Authentication
{
    public class LoginModel : PageModel
    {
        private readonly IUserRepository _userRepository;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ISession _session;

        [BindProperty(SupportsGet = true, Name = "error")]
        public string ErrorMessage { get; set; }

        public LoginModel(IUserRepository userRepository, IHttpContextAccessor contextAccessor)
        {
            _userRepository = userRepository;
            _contextAccessor = contextAccessor;
            _session = _contextAccessor.HttpContext.Session;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostLogin(string email, string password)
        {
            var user = _userRepository.Get(s => s.Email == email);
            if (user == null)
            {
                return RedirectToPage(new {error = "No Email Found!" });
            }
            if (!user.Password.Equals(password))
            {
                return RedirectToPage(new { error = "Incorrect Password!" });
            }
            var claims = new List<Claim>
            {
                new Claim("ID", user.Id.ToString()),
                new Claim(ClaimTypes.Email, email),
                new Claim(ClaimTypes.Name, user.FullName),
                new Claim("Role",user.Role.ToString())
            };
            var identity = new ClaimsIdentity(claims, "MyCookieAuth");
            var principle = new ClaimsPrincipal(identity);

            await HttpContext.SignInAsync("MyCookieAuth",principle);

            return RedirectToPage($"/Home/{(user.Role != 2 ? "UserHome" : "AdminHome")}");
        }
    }
}
