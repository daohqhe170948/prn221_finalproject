using Final_Project.Models;
using Final_Project.Repos.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;

namespace Final_Project.Pages.Schedule
{
    public class SlotViewModel : PageModel
    {
        private readonly ISlotRepository _slotRepository;
        private readonly ISlotDetailRepository _slotDetailRepository;
        private readonly IUserRepository _userRepository;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ISession _session;


        public Slot? Slot { get; set; }
        public List<SlotDetail> CurrentStudentList { get; set; } = new List<SlotDetail>();
        public List<User> AvailableMentorList { get; set; } = new List<User>();
        public List<string> ErrorMessages { get; set; } = new List<string>();

        [BindProperty(SupportsGet = true, Name = "type")]
        public int Type { get; set; }

        public User CurrentUser { get; set; }

        public SlotViewModel(ISlotRepository slotRepository, ISlotDetailRepository slotDetailRepository, IUserRepository userRepository, IHttpContextAccessor contextAccessor)
        {
            _slotRepository = slotRepository;
            _slotDetailRepository = slotDetailRepository;
            _userRepository = userRepository;
            _contextAccessor = contextAccessor;
            _session = _contextAccessor.HttpContext.Session;
        }

        public IActionResult OnGet(int slot)
        {
            if (string.IsNullOrEmpty(_session.GetString("CurrentUser"))) return RedirectToPage("/Authentication/Login");
            CurrentUser = JsonConvert.DeserializeObject<User>(_session.GetString("CurrentUser"));
            if (Type < 0 || Type > 1) Type = 0;
            var slots = _slotRepository.Gets(s => s.Id == slot);
            if(slots.Count > 0)
            {
                Slot = slots[0];
                if (Type == 0)
                {
                    var data = _slotDetailRepository.Gets(s => s.SlotId == slot);
                    CurrentStudentList = data.Where(s => s.Student?.Role == 0).ToList();
                }
                else
                {
                    var userList = _userRepository.Gets(s => s.Role == 0);
                    var slotDetailList = _slotDetailRepository.Gets(_ => true);
                    foreach (var item in userList)
                    {
                        //If student already had a subject in that slot then will not be added in the list
                        if (!slotDetailList.Any(s => s.StudentId == item.Id && s.Slot.SlotNumber == Slot.SlotNumber && s.Slot.SlotDate == Slot.SlotDate))
                        {
                            CurrentStudentList.Add(new SlotDetail { Student = item });
                        }
                    }
                }

                var mentorList = _userRepository.Gets(s => s.Role == 1);
                var slotList = _slotRepository.Gets(_ => true);
                foreach (var item in mentorList)
                {
                    //If student already had a subject in that slot then will not be added in the list
                    if (!slotList.Any(s => s.MentorId == item.Id && s.SlotNumber == Slot.SlotNumber && s.SlotDate == Slot.SlotDate))
                    {
                        AvailableMentorList.Add(item);
                    }
                }
            }
            return Page();
        }

        public void OnGetRemoveAll(int slot, int uid)
        {
            var currentSlot = _slotRepository.Get(s => s.Id == slot);
            if(currentSlot != null)
            {
                var slots = _slotRepository.Gets(s => s.SlotNumber == currentSlot.SlotNumber && s.ClassRoomId == currentSlot.ClassRoomId);
                var data = slots.Where(s => s.SlotDate.DayOfWeek.ToString().Equals(currentSlot.SlotDate.DayOfWeek.ToString()));
                foreach(var item in data)
                {
                    var slotDetail = _slotDetailRepository.Get(s => s.SlotId == item.Id && s.StudentId == uid);
                    if(slotDetail != null)
                    {
                        var result = _slotDetailRepository.Remove(slotDetail);
                        if (result == 0) ErrorMessages.Add("Remove failed");
                    }
                    else ErrorMessages.Add("Invalid slot detail id");
                }
            }
            else ErrorMessages.Add("Invalid slot id!");
            OnGet(slot);
        }

        public void OnGetAddAll(int slot, int uid)
        {
            var currentSlot = _slotRepository.Get(s => s.Id == slot);
            if (currentSlot != null)
            {
                var slots = _slotRepository.Gets(s => s.SlotNumber == currentSlot.SlotNumber && s.ClassRoomId == currentSlot.ClassRoomId);
                var data = slots.Where(s => s.SlotDate.DayOfWeek.ToString().Equals(currentSlot.SlotDate.DayOfWeek.ToString()));
                foreach(var item in data)
                {
                    var check = _slotDetailRepository.Get(s => s.StudentId == uid && s.SlotId == item.Id);
                    if(check == null)
                    {
                        var slotDetail = new SlotDetail
                        {
                            StudentId = uid,
                            SlotId = item.Id
                        };
                        var result = _slotDetailRepository.Add(slotDetail);
                        if (result == 0) ErrorMessages.Add("Add failed!");
                    }
                    else ErrorMessages.Add("This student already join this slot!");
                }
            }
            else ErrorMessages.Add("Invalid slot id!");
            OnGet(slot);
        }

        public void OnGetAdd(int slot, int uid)
        {
            var currentSlot = _slotRepository.Get(s => s.Id == slot);
            if (currentSlot != null)
            {
                var check = _slotDetailRepository.Get(s => s.StudentId == uid && s.SlotId == slot);
                if( check == null)
                {
                    var slotDetail = new SlotDetail
                    {
                        StudentId = uid,
                        SlotId = slot
                    };
                    var result = _slotDetailRepository.Add(slotDetail);
                    if (result == 0) ErrorMessages.Add("Add failed!");
                }
                else ErrorMessages.Add("This student already join this slot!");
            }
            else ErrorMessages.Add("Invalid slot id!");
            OnGet(slot);
        }

        public void OnGetRemove(int slot, int sid)
        {
            var currentSlot = _slotRepository.Get(s => s.Id == slot);
            if (currentSlot != null)
            {
                var slotDetail = _slotDetailRepository.Get(s => s.Id == sid);
                if (slotDetail != null)
                {
                    var result = _slotDetailRepository.Remove(slotDetail);
                    if (result == 0) ErrorMessages.Add("Add failed!");
                }
                else ErrorMessages.Add("Invalid slot detail id!");
            }
            else ErrorMessages.Add("Invalid slot id!");
            OnGet(slot);
        }

        public void OnGetChangeMentor(int slot, int mid)
        {
            var mentor = _userRepository.Get(s => s.Id == mid);
            if (mentor == null || mentor.Role != 1) ErrorMessages.Add("Invalid mentor ID!");
            else
            {
                var sloted = _slotRepository.Get(s => s.Id == slot);
                if (sloted != null)
                {
                    sloted.MentorId = mid;
                    _slotRepository.Update();
                }
                else ErrorMessages.Add("Invalid slot ID");
            }
            OnGet(slot);
        }
    }
}
