using Final_Project.Models;
using Final_Project.Models.OtherModels;
using Final_Project.Repos.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Final_Project.Pages.Schedule
{
    [Authorize]
    public class ViewModel : PageModel
    {
        private readonly ICsvRepository<CsvRecord> _csvRepository;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IClassroomRepository _classroomRepository;
        private readonly ISlotRepository _slotRepository;
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ISession _session;

        public Dictionary<int, List<Slot>> Schedule { get; set; } = new Dictionary<int, List<Slot>>();  
        public List<string> ImportCsvErrorMessages { get; set; }

        public ViewModel(ICsvRepository<CsvRecord> csvRepos, 
            ISubjectRepository subjectRepository, 
            IClassroomRepository classroomRepository, 
            ISlotRepository slotRepository, 
            IConfiguration configuration,
            IUserRepository userRepository,
            IHttpContextAccessor contextAccessor)
        {
            _csvRepository = csvRepos;
            _subjectRepository = subjectRepository;
            _classroomRepository = classroomRepository;
            _slotRepository = slotRepository;
            _configuration = configuration;
            _userRepository = userRepository;
            _contextAccessor = contextAccessor;
            _session = _contextAccessor.HttpContext.Session;
        }

        public void OnGet()
        {
            var startDate = DateTime.Parse(_configuration["SemesterPeriod:StartDate"]);
            var data = _slotRepository.Gets(s => s.SlotDate >= startDate && s.SlotDate <= startDate.AddDays(6));
            GetSchedule(data, Schedule);
        }
        
        public async Task OnPost(IFormFile file) 
        {
            await ProcessCsvData(file);
            OnGet();
        }

        private async Task ProcessCsvData(IFormFile file)
        {
            if (file == null || file.Length == 0) return;

            ImportCsvErrorMessages = new List<string>();
            //Get all record from csv file
            var records = _csvRepository.ReadCsv(file);
            foreach (var record in records)
            {
                if (record == null) continue;

                record.TrimData();

                //Get subject by name
                var subject = _subjectRepository.GetByName(record.SubjectName);

                //if subject invalid then cancel creating new slot
                if (subject == null)
                {
                    ImportCsvErrorMessages.Add($"Invalid subject name. Error in line {record.ToString()}");
                    continue;
                }
                var subjectId = subject.Id;

                //Get classroom by code
                var classroom = _classroomRepository.GetByCode(record.ClassroomCode);

                //if classroom is invalid then cancel creating new slot
                if (classroom == null)
                {
                    ImportCsvErrorMessages.Add($"Invalid classroom code. Error in line {record.ToString()}");
                    continue;
                }
                var classroomId = classroom.Id;

                //Check mentor email
                var mentor = _userRepository.Get(s => s.Email.Equals(record.MentorEmail) && s.Role == 1);
                if (mentor == null)
                {
                    ImportCsvErrorMessages.Add($"Cannot find mentor email. Error in line {record.ToString()}");
                    continue;
                }
                var mentorId = mentor.Id;

                //Get 2 slots
                var slotDates = record.GetSlotDate();

                //if no slot counted then csv mismatch form
                if (slotDates.Count == 0)
                {
                    ImportCsvErrorMessages.Add($"Cannot split slots. Error in line {record.ToString()}");
                    continue;
                }
                foreach (var slotDate in slotDates)
                {
                    int count = 0;
                    DateTime startDate = DateTime.Parse(_configuration["SemesterPeriod:StartDate"]);

                    //If slot is available then add new Slot
                    if (IsSlotAvailable(slotDate.slotNum, classroom.Id, slotDate.Day))
                    {
                        //Check if mentor is available for this slot
                        if (!IsUserAvailable(mentorId, slotDate.slotNum, slotDate.Day))
                        {
                            ImportCsvErrorMessages.Add($"Mentor is not available for slot {slotDate.slotNum} on {slotDate.Day}. Error in line {record.ToString()}");
                            continue;
                        }
                        while (count < (subject.Slot/2))
                        {
                            if (startDate.DayOfWeek.ToString().Equals(slotDate.Day))
                            {
                                var slot = new Slot
                                {
                                    SubjectId = subjectId,
                                    ClassRoomId = classroomId,
                                    SlotDate = startDate,
                                    SlotNumber = slotDate.slotNum,
                                    MentorId = mentorId
                                };
                                var result = await _slotRepository.CreateAsync(slot);
                                if (result == 0) ImportCsvErrorMessages.Add($"Cannot add new slot {slotDate.slotNum} on {slotDate.Day}. Error in line {record.ToString()}");
                                count++;
                            }
                            startDate = startDate.AddDays(1);
                        }
                    }
                    else ImportCsvErrorMessages.Add($"Slot {slotDate.slotNum} on {slotDate.Day} is not available. Error in line {record.ToString()}");
                }
            }
        }

        private bool IsSlotAvailable(int slotNum, int classroomId, string slotDay)
        {
            var slot = _slotRepository.Gets(s => s.SlotNumber == slotNum && s.ClassRoomId == classroomId);
            var data = slot.Find(s => s.SlotDate.DayOfWeek.ToString().Equals(slotDay));
            return data == null;
        }

        private void GetSchedule(List<Slot> data, Dictionary<int, List<Slot>> schedule)
        {
            var mondays = new List<Slot>();
            var tuesdays = new List<Slot>();
            var wednesdays = new List<Slot>();
            var thursdays = new List<Slot>();
            var fridays = new List<Slot>();
            var saturdays = new List<Slot>();
            var sundays = new List<Slot>();

            foreach(var item in data)
            {
                switch(item.SlotDate.DayOfWeek.ToString())
                {
                    case "Monday":
                        {
                            mondays.Add(item);
                            break;
                        }
                    case "Tuesday":
                        {
                            tuesdays.Add(item);
                            break;
                        }
                    case "Wednesday":
                        {
                            wednesdays.Add(item);
                            break;
                        }
                    case "Thursday":
                        {
                            thursdays.Add(item);
                            break;
                        }
                    case "Friday":
                        {
                            fridays.Add(item);
                            break;
                        }
                    case "Saturday":
                        {
                            saturdays.Add(item);
                            break;
                        }
                    case "Sunday":
                        {
                            sundays.Add(item);
                            break;
                        }
                }
            }

            schedule.Add(2, mondays);
            schedule.Add(3, tuesdays);
            schedule.Add(4, wednesdays);
            schedule.Add(5, thursdays);
            schedule.Add(6, fridays);
            schedule.Add(7, saturdays);
            schedule.Add(8, sundays);
        }

        private bool IsUserAvailable(int mentorId, int slotNum, string slotDay)
        {
            var slot = _slotRepository.Gets(s => s.SlotNumber == slotNum && s.MentorId == mentorId);
            if(slot.Count == 0) return true;
            var data = slot.Find(s => s.SlotDate.DayOfWeek.ToString().Equals(slotDay));
            return data == null;
        }
    }
}
