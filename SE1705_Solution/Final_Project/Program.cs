using Final_Project.Models;
using Final_Project.Models.OtherModels;
using Final_Project.Repos.Implement;
using Final_Project.Repos.Interface;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddDbContext<PRN221_FinalProjectContext>(option =>
    option.UseSqlServer(builder.Configuration.GetConnectionString(("SqlServer")),
    sqlServerOptions => sqlServerOptions.EnableRetryOnFailure())
);

builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Services.AddTransient<ISubjectRepository, SubjectRepository>();
builder.Services.AddTransient<ISlotRepository, SlotRepository>();
builder.Services.AddTransient<IClassroomRepository, ClassroomRepository>();
builder.Services.AddTransient<ICsvRepository<CsvRecord>, CsvRepository<CsvRecord>>();
builder.Services.AddTransient<ISlotDetailRepository, SlotDetailRepository>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

builder.Services.AddAuthentication("MyCookieAuth").AddCookie("MyCookieAuth", option =>
{
    option.Cookie.Name = "MyCookieAuth";
    option.ExpireTimeSpan = TimeSpan.FromMinutes(1);
    option.LoginPath = "/Authentication/Unauthenticated";
    option.AccessDeniedPath = "/Authentication/AccessDenied";
});

builder.Services.AddAuthorization(option =>
{
    option.AddPolicy("Admin", policy =>
    {
        policy.RequireClaim("Role", "2");
    });
    option.AddPolicy("Student", policy =>
    {
        policy.RequireClaim("Role", "0");
    });
});

builder.Services.AddSession(cfg =>
{
    cfg.IdleTimeout = TimeSpan.FromMinutes(10);
    cfg.Cookie.HttpOnly = true;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseSession();

app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.UseEndpoints(option =>
{
    option.MapFallbackToPage("/Authentication/Login");
});

app.Run();
