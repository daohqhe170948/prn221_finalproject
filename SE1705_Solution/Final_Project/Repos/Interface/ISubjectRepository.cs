﻿using Final_Project.Models;
using System.Linq.Expressions;

namespace Final_Project.Repos.Interface
{
    public interface ISubjectRepository
    {
        List<Subject> GetAll();
        List<Subject> Get(Expression<Func<Subject, bool>> predicate);
        Subject? GetByName(string name);
    }
}
