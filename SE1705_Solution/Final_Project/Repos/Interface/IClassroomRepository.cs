﻿using Final_Project.Models;

namespace Final_Project.Repos.Interface
{
    public interface IClassroomRepository
    {
        public ClassRoom GetByCode(string code);
    }
}
