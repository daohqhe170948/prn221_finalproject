﻿using Final_Project.Models;
using System.Linq.Expressions;

namespace Final_Project.Repos.Interface
{
    public interface ISlotDetailRepository
    {
        List<SlotDetail> Gets(Expression<Func<SlotDetail,bool>> predicate);
        int Remove(SlotDetail slotDetail);
        int Add(SlotDetail slotDetail);
        SlotDetail? Get(Expression<Func<SlotDetail, bool>> predicate);
    }
}
