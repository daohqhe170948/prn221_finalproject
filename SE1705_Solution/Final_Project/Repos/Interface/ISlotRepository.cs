﻿using Final_Project.Models;
using System.Linq.Expressions;

namespace Final_Project.Repos.Interface
{
    public interface ISlotRepository
    {
        Task<int> CreateAsync(Slot slot);
        List<Slot> Gets(Expression<Func<Slot, bool>> predicate);
        Slot? Get(Expression<Func<Slot, bool>> predicate);
        void Update();
    }
}
