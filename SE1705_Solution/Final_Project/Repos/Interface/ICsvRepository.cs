﻿namespace Final_Project.Repos.Interface
{
    public interface ICsvRepository<T> where T : class
    {
        List<T> ReadCsv(IFormFile file);
    }
}
