﻿using Final_Project.Models;
using System.Linq.Expressions;

namespace Final_Project.Repos.Interface
{
    public interface IUserRepository
    {
        List<User> GetAll();
        User? Get(Expression<Func<User,bool>> predicate);
        List<User> Gets(Expression<Func<User, bool>> predicate);
    }
}
