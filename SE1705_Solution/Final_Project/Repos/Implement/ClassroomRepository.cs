﻿using Final_Project.Models;
using Final_Project.Repos.Interface;

namespace Final_Project.Repos.Implement
{
    public class ClassroomRepository : BaseRepository, IClassroomRepository
    {
        public ClassroomRepository(PRN221_FinalProjectContext context) : base(context) { }

        public ClassRoom? GetByCode(string code)
        {
            return _dbContext.ClassRooms.FirstOrDefault(r => r.Code == code);
        }
    }
}
