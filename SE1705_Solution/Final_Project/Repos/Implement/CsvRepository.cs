﻿using CsvHelper.Configuration;
using CsvHelper;
using Final_Project.Repos.Interface;
using System.Globalization;
using Final_Project.Models.OtherModels;

namespace Final_Project.Repos.Implement
{
    public class CsvRepository<T> : ICsvRepository<T> where T : class
    {
        public List<T> ReadCsv(IFormFile file)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false // Assuming there's no header in the CSV file
            };
            using (var reader = new StreamReader(file.OpenReadStream()))
            using (var csv = new CsvReader(reader, config))
            {
                return csv.GetRecords<T>().ToList();
            }
        }
    }
}
