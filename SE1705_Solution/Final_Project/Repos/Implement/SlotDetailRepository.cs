﻿using Final_Project.Models;
using Final_Project.Repos.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Final_Project.Repos.Implement
{
    public class SlotDetailRepository : BaseRepository, ISlotDetailRepository
    {
        public SlotDetailRepository(PRN221_FinalProjectContext dbContext) : base(dbContext) { }

        public List<SlotDetail> Gets(Expression<Func<SlotDetail, bool>> predicate)
        {
            return _dbContext.SlotDetails.Where(predicate).Include(s => s.Student).Include(s => s.Slot).ToList();
        }

        public int Remove(SlotDetail slotDetail)
        {
            _dbContext.SlotDetails.Remove(slotDetail);
            return _dbContext.SaveChanges();
        }

        public int Add(SlotDetail slotDetail)
        {
            _dbContext.SlotDetails.Add(slotDetail);
            return _dbContext.SaveChanges();
        }

        public SlotDetail? Get(Expression<Func<SlotDetail, bool>> predicate)
        {
            return _dbContext.SlotDetails.FirstOrDefault(predicate);
        }
    }
}
