﻿using Final_Project.Models;
using Final_Project.Repos.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Final_Project.Repos.Implement
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(PRN221_FinalProjectContext context) : base(context) { }

        public List<User> GetAll()
        {
            return _dbContext.Users.ToList();
        }

        public User? Get(Expression<Func<User,bool>> predicate)
        {
            return _dbContext.Users.FirstOrDefault(predicate);
        }

        public List<User> Gets(Expression<Func<User, bool>> predicate)
        {
            return _dbContext.Users.Where(predicate).Include(s => s.SlotDetails).ToList();  
        }
    }
}
