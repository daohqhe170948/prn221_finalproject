﻿using Final_Project.Models;
using Final_Project.Repos.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Final_Project.Repos.Implement
{
    public class SlotRepository : BaseRepository, ISlotRepository
    {
        public SlotRepository(PRN221_FinalProjectContext context) : base(context) { }

        public async Task<int> CreateAsync(Slot slot)
        {
            await _dbContext.Slots.AddAsync(slot);
            return  await _dbContext.SaveChangesAsync();
        }

        public List<Slot> Gets(Expression<Func<Slot, bool>> predicate)
        {
            return _dbContext.Slots.Where(predicate).Include(s => s.Subject).Include(s => s.ClassRoom).Include(s => s.Mentor).Include(s => s.SlotDetails).OrderBy(s => s.SlotNumber).ToList();
        }

        public Slot? Get(Expression<Func<Slot, bool>> predicate)
        {
            return _dbContext.Slots.FirstOrDefault(predicate);
        }

        public void Update()
        {
            _dbContext.SaveChanges();
        }
    }
}