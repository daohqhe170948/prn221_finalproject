﻿using Final_Project.Models;

namespace Final_Project.Repos.Implement
{
    public class BaseRepository
    {
        protected readonly PRN221_FinalProjectContext _dbContext;

        public BaseRepository(PRN221_FinalProjectContext dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
