﻿using Final_Project.Models;
using Final_Project.Repos.Interface;
using System.Linq.Expressions;

namespace Final_Project.Repos.Implement
{
    public class SubjectRepository : BaseRepository, ISubjectRepository
    {
        public SubjectRepository(PRN221_FinalProjectContext context) : base(context) { }

        public List<Subject> GetAll()
        {
            return _dbContext.Subjects.ToList();
        }
        public List<Subject> Get(Expression<Func<Subject, bool>> predicate)
        {
            return _dbContext.Subjects.Where(predicate).ToList();
        }

        public Subject? GetByName(string name)
        {
            return _dbContext.Subjects.FirstOrDefault(x => x.Name == name);
        }
    }
}
