﻿using System;
using System.Collections.Generic;

namespace Final_Project.Models
{
    public partial class SlotDetail
    {
        public int Id { get; set; }
        public int? SlotId { get; set; }
        public int? StudentId { get; set; }
        public int? Status { get; set; }

        public virtual Slot? Slot { get; set; }
        public virtual User? Student { get; set; }
    }
}
