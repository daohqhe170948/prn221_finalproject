﻿using System;
using System.Collections.Generic;

namespace Final_Project.Models
{
    public partial class Slot
    {
        public Slot()
        {
            SlotDetails = new HashSet<SlotDetail>();
        }

        public int Id { get; set; }
        public int? MentorId { get; set; }
        public int? SubjectId { get; set; }
        public int? ClassRoomId { get; set; }
        public int SlotNumber { get; set; }
        public DateTime SlotDate { get; set; }

        public virtual ClassRoom? ClassRoom { get; set; }
        public virtual User? Mentor { get; set; }
        public virtual Subject? Subject { get; set; }
        public virtual ICollection<SlotDetail> SlotDetails { get; set; }
    }
}
