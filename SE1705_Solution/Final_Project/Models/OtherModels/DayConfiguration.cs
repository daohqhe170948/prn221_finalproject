﻿namespace Final_Project.Models.OtherModels
{
    public class DayConfiguration
    {
        public static readonly Dictionary<int, string> DayMapper = new Dictionary<int, string>
        {
            { 2, "Monday" }, { 3, "Tuesday" }, { 4, "Wednesday" }, { 5, "Thursday" }, { 6, "Friday" }, { 7, "Saturday" }, { 8, "Sunday" }
        };
    }
}
