﻿namespace Final_Project.Models.OtherModels
{
    public class CsvRecord
    {
        public string SubjectName { get; set; }
        public string Slot { get; set; }       
        public string ClassroomCode { get; set; }
        public string MentorEmail { get; set; }

        public List<SlotDate> GetSlotDate()
        {
            var data = Slot.ToCharArray();
            List<SlotDate> listSlot = new List<SlotDate>();
            if(data.Length != 3) return listSlot;

            if (data[0].Equals('A') || data[0].Equals('a'))
            {
                listSlot.Add(new SlotDate
                {
                    Day = DayConfiguration.DayMapper.GetValueOrDefault(Int32.Parse(data[1].ToString())),
                    slotNum = 1
                });
                listSlot.Add(new SlotDate
                {
                    Day = DayConfiguration.DayMapper.GetValueOrDefault(Int32.Parse(data[2].ToString())),
                    slotNum = 2
                });
            }
            else
            {
                listSlot.Add(new SlotDate
                {
                    Day = DayConfiguration.DayMapper.GetValueOrDefault(Int32.Parse(data[1].ToString())),
                    slotNum = 3
                });
                listSlot.Add(new SlotDate
                {
                    Day = DayConfiguration.DayMapper.GetValueOrDefault(Int32.Parse(data[2].ToString())),
                    slotNum = 4
                });
            }
            return listSlot;
        }

        public void TrimData()
        {
            this.SubjectName = SubjectName.Trim();
            this.Slot = Slot.Trim();
            this.ClassroomCode = ClassroomCode.Trim();
            this.MentorEmail = MentorEmail.Trim();
        }

        public override string ToString()
        {
            return $"{SubjectName}; {Slot}; {ClassroomCode}; {MentorEmail}";
        }
    }

    public record SlotDate
    {
        public string Day { get; set; }
        public int slotNum { get; set; }
    }
}
