﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Final_Project.Models
{
    public partial class PRN221_FinalProjectContext : DbContext
    {
        public PRN221_FinalProjectContext()
        {
        }

        public PRN221_FinalProjectContext(DbContextOptions<PRN221_FinalProjectContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClassRoom> ClassRooms { get; set; } = null!;
        public virtual DbSet<Slot> Slots { get; set; } = null!;
        public virtual DbSet<SlotDetail> SlotDetails { get; set; } = null!;
        public virtual DbSet<Subject> Subjects { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClassRoom>(entity =>
            {
                entity.ToTable("ClassRoom");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code).HasMaxLength(20);
            });

            modelBuilder.Entity<Slot>(entity =>
            {
                entity.ToTable("Slot");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClassRoomId).HasColumnName("ClassRoomID");

                entity.Property(e => e.MentorId).HasColumnName("MentorID");

                entity.Property(e => e.SlotDate).HasColumnType("date");

                entity.Property(e => e.SubjectId).HasColumnName("SubjectID");

                entity.HasOne(d => d.ClassRoom)
                    .WithMany(p => p.Slots)
                    .HasForeignKey(d => d.ClassRoomId)
                    .HasConstraintName("FK__Slot__ClassRoomI__3F466844");

                entity.HasOne(d => d.Mentor)
                    .WithMany(p => p.Slots)
                    .HasForeignKey(d => d.MentorId)
                    .HasConstraintName("FK__Slot__UserID__3D5E1FD2");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.Slots)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK__Slot__SubjectID__3E52440B");
            });

            modelBuilder.Entity<SlotDetail>(entity =>
            {
                entity.ToTable("SlotDetail");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.SlotId).HasColumnName("SlotID");

                entity.Property(e => e.StudentId).HasColumnName("StudentID");

                entity.HasOne(d => d.Slot)
                    .WithMany(p => p.SlotDetails)
                    .HasForeignKey(d => d.SlotId)
                    .HasConstraintName("FK__SlotDetai__SlotI__4D94879B");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.SlotDetails)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__SlotDetai__Stude__4E88ABD4");
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.ToTable("Subject");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(20);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
