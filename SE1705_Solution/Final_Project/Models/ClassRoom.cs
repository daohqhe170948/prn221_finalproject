﻿using System;
using System.Collections.Generic;

namespace Final_Project.Models
{
    public partial class ClassRoom
    {
        public ClassRoom()
        {
            Slots = new HashSet<Slot>();
        }

        public int Id { get; set; }
        public string Code { get; set; } = null!;

        public virtual ICollection<Slot> Slots { get; set; }
    }
}
