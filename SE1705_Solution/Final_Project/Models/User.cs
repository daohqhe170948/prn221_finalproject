﻿using System;
using System.Collections.Generic;

namespace Final_Project.Models
{
    public partial class User
    {
        public User()
        {
            SlotDetails = new HashSet<SlotDetail>();
            Slots = new HashSet<Slot>();
        }

        public int Id { get; set; }
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public int Role { get; set; }
        public string FullName { get; set; } = null!;

        public virtual ICollection<SlotDetail> SlotDetails { get; set; }
        public virtual ICollection<Slot> Slots { get; set; }
    }
}
