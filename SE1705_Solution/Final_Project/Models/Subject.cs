﻿using System;
using System.Collections.Generic;

namespace Final_Project.Models
{
    public partial class Subject
    {
        public Subject()
        {
            Slots = new HashSet<Slot>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int Slot { get; set; }

        public virtual ICollection<Slot> Slots { get; set; }
    }
}
