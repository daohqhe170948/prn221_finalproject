﻿using Lab2_Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Lab2_Server.Repos.Implements;
using Lab2_Server.Repos.Repositories;
using System.Net.Sockets;
using System.Net;
using Microsoft.VisualBasic;
using Lab2_Server.Models.DTOs;
using Newtonsoft.Json;
using AutoMapper;
using Lab2_Server.Extentions;
using System.Runtime.CompilerServices;

namespace Lab2_Server
{
    internal class Program
    {
        static int get_all_type; static int create_type; static int delete_type; static int update_type; static int search_type;
        static int numberOfClient = 0;

        private static IServiceProvider _serviceProvider;
        public static IConfiguration Configuration { get; private set; }
        private static IMapper Mapper;

        static void Main(string[] args)
        {
            Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            get_all_type = Int32.Parse(Configuration["RequestType:GetAll"] ?? String.Empty);
            create_type = Int32.Parse(Configuration["RequestType:Create"] ?? String.Empty);
            delete_type = Int32.Parse(Configuration["RequestType:Delete"] ?? String.Empty);
            update_type = Int32.Parse(Configuration["RequestType:Update"] ?? String.Empty);
            search_type = Int32.Parse(Configuration["RequestType:Search"] ?? String.Empty);

            _serviceProvider = ConfigureServices();
            string host = Configuration["ServerConnection:ServerIP"] ?? String.Empty;
            int port = Int32.Parse(Configuration["ServerConnection:ServerPort"] ?? String.Empty);
            Console.WriteLine("Server App");
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener server = new TcpListener(localAddr, port);
            server.Start();

            Console.WriteLine("************************");
            Console.WriteLine("waiting....");

            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                Console.Write("*************************");
                Console.WriteLine($"Number of client connected: {++numberOfClient}");
                Thread thread = new Thread(new ParameterizedThreadStart(ProcessClient));
                thread.Start(client);
            }
        }

        static void ProcessClient(object parameter)
        {
            var repos = _serviceProvider.GetRequiredService<ICrudRepository<Product>>();
            var orderDetailRepos = _serviceProvider.GetRequiredService<OrderDetailRepos>();

            int clientID = numberOfClient;
            TcpClient client = (TcpClient)parameter;
            string dataString;
            ResponseModel<dynamic>? response = null;
            int count;
            NetworkStream stream = client.GetStream();

            Byte[] bytes = new Byte[2048];
            try
            {
                while ((count = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    dataString = System.Text.Encoding.ASCII.GetString(bytes, 0, count);
                    Console.WriteLine($"Data received from client {clientID} at {DateTime.Now}");

                    RequestModel<ProductDTO>? data = null;
                    //Deserialize to object
                    try
                    {
                        data = JsonConvert.DeserializeObject<RequestModel<ProductDTO>>(dataString);
                        if (data == null || data.Type == null)
                        {
                            response = new ResponseModel<dynamic>
                            {
                                Type = -1,
                                Data = null,
                                Message = "Request data invalid"
                            };
                            ResponseMessage(clientID, response, stream);
                        }
                        else
                        {
                            var type = data.Type;
                            if (type == get_all_type)
                            {
                                response = new ResponseModel<dynamic>
                                {
                                    Type = get_all_type,
                                    Data = Mapper.Map<List<Product>, List<ProductDTO>>(repos.GetAll()),
                                    Message = "Load data completed"
                                };
                                ResponseMessage(clientID, response, stream);
                            }
                            else if (type == create_type)
                            {
                                response = new ResponseModel<dynamic>
                                {
                                    Type = create_type,
                                    Data = null,
                                    Message = repos.Add(Mapper.Map<Product>(data.Data)) ? "Crete product success" : "Crete product fail"
                                };
                                ResponseMessage(clientID, response, stream);
                            }
                            else if (type == delete_type)
                            {
                                var orderDetailList = orderDetailRepos.GetOrderDetails(data.Data.ProductId??0);
                                if (orderDetailList.Count > 0)
                                {
                                    foreach(var item in orderDetailList)
                                    {
                                        orderDetailRepos.Delete(item);
                                    }
                                }
                                response = new ResponseModel<dynamic>
                                {
                                    Type = delete_type,
                                    Data = null,
                                    Message = repos.Delete(data.Data.ProductId)
                                };
                                ResponseMessage(clientID, response, stream);
                            }
                            else if (type == update_type)
                            {
                                response = new ResponseModel<dynamic>
                                {
                                    Type = update_type,
                                    Data = null,
                                    Message = repos.Update(Mapper.Map<Product>(data.Data))
                                };
                                ResponseMessage(clientID, response, stream);
                            }
                            else if (type == search_type)
                            {
                                var result = Mapper.Map<Product, ProductDTO>(repos.GetById(data.SearchId ?? 0));
                                response = new ResponseModel<dynamic>
                                {
                                    Type = search_type,
                                    Data = result,
                                    Message = result == null ? "No result found" : "Load data complete"
                                };
                                ResponseMessage(clientID, response, stream);
                            }
                            else
                            {
                                response = new ResponseModel<dynamic>
                                {
                                    Type = -1,
                                    Data = null,
                                    Message = "Request data invalid"
                                };
                                ResponseMessage(clientID, response, stream);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        response = new ResponseModel<dynamic>
                        {
                            Type = -1,
                            Data = null,
                            Message = ex.Message
                        };
                        ResponseMessage(clientID, response, stream);
                    }
                }
                if (!client.Connected)
                {
                    Console.WriteLine($"Client {clientID} at address {((IPEndPoint)client.Client.RemoteEndPoint).Address} has closed connection______________________");
                    numberOfClient--;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Client {clientID} at address {((IPEndPoint)client.Client.RemoteEndPoint).Address} has closed connection______________________");
                numberOfClient--;
            }
        }

        private static void ResponseMessage(int clientID, ResponseModel<dynamic> response, NetworkStream stream)
        {
            Console.WriteLine($"Data response to client {clientID} at {DateTime.Now}");
            string responseString = JsonConvert.SerializeObject(response);
            Byte[] bytes = System.Text.Encoding.ASCII.GetBytes(responseString);
            stream.Write(bytes, 0, bytes.Length);
        }

        private static IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();

            services.AddDbContext<NorthwindContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("MyCnn"),
                sqlServerOptions => sqlServerOptions.EnableRetryOnFailure()));

            services.AddScoped<ICrudRepository<Product>, CrudRepository<Product>>();
            services.AddScoped<ICrudRepository<OrderDetail>, CrudRepository<OrderDetail>>();
            services.AddScoped<OrderDetailRepos, OrderDetailRepository>();

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            Mapper = mapperConfig.CreateMapper();

            return services.BuildServiceProvider();
        }
    }
}