﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Server.Models.DTOs
{
    public class RequestModel<T> where T : class
    {
        public int? Type { get; set; }
        public T? Data { get; set; }
        public int? SearchId { get; set; }
    }
}
