﻿using System;
using System.Collections.Generic;

namespace Lab2_Server.Models
{
    public partial class OrderSubtotal
    {
        public int OrderId { get; set; }
        public decimal? Subtotal { get; set; }
    }
}
