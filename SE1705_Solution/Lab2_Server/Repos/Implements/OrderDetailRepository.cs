﻿using Lab2_Server.Models;
using Lab2_Server.Repos.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Server.Repos.Implements
{
    public class OrderDetailRepository : OrderDetailRepos
    {
        private readonly NorthwindContext dbContext;

        public OrderDetailRepository(NorthwindContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<OrderDetail> GetOrderDetails(int productId)
        {
            return dbContext.OrderDetails.Where(o => o.ProductId == productId).ToList();
        }

        public void Delete(OrderDetail orderDetail)
        {
            dbContext.Remove(orderDetail);
            dbContext.SaveChanges();
        }
    }
}
