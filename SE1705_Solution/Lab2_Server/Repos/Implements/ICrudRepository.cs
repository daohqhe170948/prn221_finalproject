﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Server.Repos.Implements
{
    public interface ICrudRepository<TEntity> where TEntity : class
    {
        TEntity? GetById(object id);
        List<TEntity> GetAll();
        List<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        bool Add(TEntity? entity);
        string Update(TEntity? entity);
        string Delete(object? id);
    }
}
