﻿using Lab2_Server.Models;
using Lab2_Server.Repos.Implements;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Server.Repos.Repositories
{
    public class CrudRepository<TEntity> : ICrudRepository<TEntity> where TEntity : class
    {
        private readonly NorthwindContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public CrudRepository(NorthwindContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public bool Add(TEntity? entity)
        {
            if (entity == null) return false;
            try
            {
                _dbSet.Add(entity);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public string Delete(object? id)
        {
            if (id == null) return "id null";
            var target = GetById(id);
            if(target != null)
            {
                try
                {
                    _dbSet.Remove(target);
                    _context.SaveChanges();
                    return "success";
                }
                catch(Exception ex)
                {
                    return ex.Message;
                }
            }
            return "target null";
        }

        public List<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public List<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public TEntity? GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public string Update(TEntity? entity)
        {
            if (entity == null) return "null";
            try
            {
                _context.Update(entity);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "success";
        }
    }
}
