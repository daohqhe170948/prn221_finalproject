﻿using Lab2_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Server.Repos.Repositories
{
    public interface OrderDetailRepos
    {
        public List<OrderDetail> GetOrderDetails(int productId);
        public void Delete(OrderDetail orderDetail);
    }
}
